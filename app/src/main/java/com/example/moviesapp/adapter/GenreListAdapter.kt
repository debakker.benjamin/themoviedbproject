package com.example.moviesapp.adapter

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.ScaleAnimation
import android.widget.ImageView
import android.widget.TextView
import androidx.core.app.ActivityOptionsCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.moviesapp.R
import com.example.moviesapp.activity.DetailActivity
import com.example.moviesapp.entities.Genre
import com.example.moviesapp.entities.Result
import com.squareup.picasso.Picasso

class GenreListAdapter(var genreList: List<Genre>) :
    RecyclerView.Adapter<GenreListAdapter.MyViewHolder>() {

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var genreTv: TextView = view.findViewById(R.id.genre)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_genre_list, parent, false)


        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val genreItem = genreList[position]
        holder.genreTv.text = genreItem.name

    }

    override fun getItemCount(): Int {
        return genreList.size
    }
}
