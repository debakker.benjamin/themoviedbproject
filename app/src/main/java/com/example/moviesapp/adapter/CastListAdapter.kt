package com.example.moviesapp.adapter

import android.media.Image
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.moviesapp.R
import com.example.moviesapp.entities.Cast
import com.example.moviesapp.entities.Credit
import com.example.moviesapp.entities.Genre
import com.squareup.picasso.Picasso


class CastListAdapter(var castList: List<Cast>) :
    RecyclerView.Adapter<CastListAdapter.MyViewHolder>() {

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var castImage: ImageView = view.findViewById(R.id.cast_image)
        var castName: TextView = view.findViewById(R.id.name_cast)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_cast_list, parent, false)

        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val castItem = castList[position]

        Picasso.get().load("https://image.tmdb.org/t/p/w200/${castItem.profile_path}")
            .placeholder(R.drawable.placeholder)
            .into(holder.castImage)

        holder.castName.text = castItem.name

    }

    override fun getItemCount(): Int {
        return castList.size
    }
}