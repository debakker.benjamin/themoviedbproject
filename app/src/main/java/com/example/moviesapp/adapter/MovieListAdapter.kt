package com.example.moviesapp.adapter

import android.app.Activity
import android.content.Context
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.ScaleAnimation
import android.widget.ImageView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.example.moviesapp.R
import com.example.moviesapp.entities.Result
import com.squareup.picasso.MemoryPolicy
import com.squareup.picasso.NetworkPolicy
import com.squareup.picasso.Picasso
import androidx.core.content.ContextCompat.startActivity
import androidx.core.view.ViewCompat
import androidx.core.app.ActivityOptionsCompat
import android.content.Intent
import com.example.moviesapp.activity.DetailActivity
import com.example.moviesapp.fragment.HomeFragment
import android.R.id.edit
import android.content.Context.MODE_PRIVATE
import android.widget.Toast

class MovieListAdapter(
    var resultList: List<Result>,
    private val context: Context,
    private val activity: Activity
) :
    RecyclerView.Adapter<MovieListAdapter.MyViewHolder>() {

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var imageView: ImageView = view.findViewById(R.id.img_movie)
        var icFavorite: ImageView = view.findViewById(R.id.ic_fav)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_movie_list, parent, false)


        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val resultItem = resultList[position]

        // on regarde dans les pref si on a l'item en favoris
        val pref = context.getSharedPreferences("MoviePref", MODE_PRIVATE)
        val editor = pref.edit().apply {
            if (pref.getBoolean(resultItem.id.toString(), false)) {
                holder.icFavorite.setImageResource(R.drawable.ic_star_fav_active)
            } else {
                holder.icFavorite.setImageResource(R.drawable.ic_star_fav)
            }
        }.apply()

        //setScaleAnimation(holder.itemView)
        holder.icFavorite.setOnClickListener {

            val editorChange = pref.edit().apply {
                if (pref.getBoolean(resultItem.id.toString(), false)) {
                    holder.icFavorite.setImageResource(R.drawable.ic_star_fav)
                    remove(resultItem.id.toString())
                    Toast.makeText(context, "Supprimer des favoris", Toast.LENGTH_SHORT).show()

                } else {
                    holder.icFavorite.setImageResource(R.drawable.ic_star_fav_active)
                    putBoolean(resultItem.id.toString(), true)
                    Toast.makeText(context, "Ajouter aux favoris", Toast.LENGTH_SHORT).show()
                }
            }.apply()
        }

        Picasso.get().load("https://image.tmdb.org/t/p/w200/${resultItem.poster_path}")
            .placeholder(R.drawable.placeholder)
            .into(holder.imageView)

        holder.imageView.setOnClickListener {
            val intent = Intent(context, DetailActivity::class.java)
            intent.putExtra("resultItemId", resultItem.id)
            val options =
                ActivityOptionsCompat.makeSceneTransitionAnimation(
                    activity,
                    holder.imageView,
                    "detail"
                )
            startActivity(context, intent, options.toBundle())

        }
    }

    override fun getItemCount(): Int {
        return resultList.size
    }

    private fun setScaleAnimation(view: View) {
        val anim = ScaleAnimation(
            0.0f,
            1.0f,
            0.0f,
            1.0f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        anim.duration = 500
        view.startAnimation(anim)
    }
}
