package com.example.moviesapp.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.moviesapp.R
import com.example.moviesapp.entities.Cast
import com.example.moviesapp.entities.Result
import com.squareup.picasso.Picasso


class SimilarMovieListAdapter(var similarList: List<Result>) :
    RecyclerView.Adapter<SimilarMovieListAdapter.MyViewHolder>() {

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var ViewPagerImage: ImageView = view.findViewById(R.id.image_view_pager)
        var titleMovie: TextView = view.findViewById(R.id.title_movie)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_viewpager_list, parent, false)

        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val similarItem = similarList[position]

        Picasso.get().load("https://image.tmdb.org/t/p/w400/${similarItem.backdrop_path}")
            .placeholder(R.drawable.placeholder)
            .into(holder.ViewPagerImage)

        holder.titleMovie.text = similarItem.title

    }

    override fun getItemCount(): Int {
        return similarList.size
    }
}