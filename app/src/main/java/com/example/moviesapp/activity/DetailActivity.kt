package com.example.moviesapp.activity

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import com.example.moviesapp.R
import com.example.moviesapp.adapter.CastListAdapter
import com.example.moviesapp.adapter.GenreListAdapter
import com.example.moviesapp.adapter.SimilarMovieListAdapter
import com.example.moviesapp.entities.Cast
import com.example.moviesapp.entities.Genre
import com.example.moviesapp.entities.Result
import com.example.moviesapp.viewmodel.HomeViewModel
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import io.reactivex.disposables.Disposable
import java.lang.Exception
import android.content.Intent
import android.net.Uri
import android.view.Menu
import androidx.appcompat.widget.Toolbar
import com.example.moviesapp.utils.Constants.VIDEO_TRAILER_BASE
import android.view.MenuItem
import android.widget.Toast
import com.example.moviesapp.adapter.MovieListAdapter
import com.example.moviesapp.fragment.HomeFragment
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.io.IOException
import java.net.InetSocketAddress
import java.net.Socket

class DetailActivity : AppCompatActivity() {

    private var bannerMovie: ImageView? = null
    private var bannerBack: ImageView? = null
    private var starTv: TextView? = null
    private var releaseDateTv: TextView? = null
    private var recyclerViewGenre: RecyclerView? = null
    private var resultItem: Result? = null
    private var movieIdFromExtra: Int? = null
    private var getGenreFromRoomDisposable: Disposable? = null
    private var resultDisposable: Disposable? = null
    private var fetchCreditDisposable: Disposable? = null
    private var getCreditDisposable: Disposable? = null
    private var fetchSimilarDisposable: Disposable? = null
    private var getSimilarDisposable: Disposable? = null
    private var fetchVideoDisposable: Disposable? = null
    private var getVideoDisposable: Disposable? = null
    private var listGenreIds: ArrayList<Int>? = null
    private var listGenre: ArrayList<Genre>? = null
    private var genreAdapter: GenreListAdapter? = null
    private var llManagerGenre: LinearLayoutManager? = null
    private var llManagerCast: LinearLayoutManager? = null
    private var descMovieTv: TextView? = null
    private var recyclerCast: RecyclerView? = null
    private var castAdapter: CastListAdapter? = null
    private var listCast: ArrayList<Cast>? = null
    private var viewPagerSimilarMovie: ViewPager2? = null
    private var viewPagerAdapterSimilarMovie: SimilarMovieListAdapter? = null
    private var btnPrevPager: Button? = null
    private var btnNextPager: Button? = null
    private var btnPlay: Button? = null
    private var isActiveFav: Boolean = false
    private var checkInternetDisposable: Disposable? = null

    private val homeViewModel by lazy {
        ViewModelProviders.of(this).get(HomeViewModel::class.java)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu, menu)

        val pref = applicationContext.getSharedPreferences("MoviePref", Context.MODE_PRIVATE)
        val editor = pref.edit().apply {
            if (pref.getBoolean(movieIdFromExtra.toString(), false)) {
                menu.findItem(R.id.toolbar_ic).setIcon(R.drawable.ic_star_fav_active)
            } else {
                menu.findItem(R.id.toolbar_ic).setIcon(R.drawable.ic_star_fav)
            }
        }.apply()
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.toolbar_ic -> {
                val pref = applicationContext.getSharedPreferences("MoviePref", Context.MODE_PRIVATE)
                val editorChange = pref.edit().apply {
                    if (pref.getBoolean(movieIdFromExtra.toString(), false)) {
                        item.setIcon(R.drawable.ic_star_fav)
                        remove(movieIdFromExtra.toString())
                        Toast.makeText(applicationContext, "Supprimer des favoris", Toast.LENGTH_SHORT).show()

                    } else {
                        item.setIcon(R.drawable.ic_star_fav_active)
                        putBoolean(movieIdFromExtra.toString(), true)
                        Toast.makeText(applicationContext, "Ajouter aux favoris", Toast.LENGTH_SHORT).show()
                    }
                }.apply()

            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)

        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_arrow_back)

        bannerMovie = findViewById(R.id.banner)
        bannerBack = findViewById(R.id.banner_back)
        starTv = findViewById(R.id.star)
        releaseDateTv = findViewById(R.id.release_date)
        recyclerViewGenre = findViewById(R.id.recycler_genres)
        descMovieTv = findViewById(R.id.desc_movie)
        recyclerCast = findViewById(R.id.recyclerview_cast)
        viewPagerSimilarMovie = findViewById(R.id.view_pager_similar_movies)
        listGenreIds = ArrayList()
        listGenre = ArrayList()
        listCast = ArrayList()
        btnNextPager = findViewById(R.id.next_btn_pager)
        btnPrevPager = findViewById(R.id.prev_btn_pager)
        btnPlay = findViewById(R.id.ic_play)

        // set up recycler view genre
        genreAdapter = GenreListAdapter(listGenre!!)
        llManagerGenre = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        recyclerViewGenre?.layoutManager = llManagerGenre
        recyclerViewGenre?.adapter = genreAdapter

        // set up recycler view cast
        castAdapter = CastListAdapter(listCast!!)
        llManagerCast = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        recyclerCast?.layoutManager = llManagerCast
        recyclerCast?.adapter = castAdapter

        val b = intent.extras
        // pour la transition avec transitionName
        supportPostponeEnterTransition()

        if (b != null) {
            //recupération de l'id par l'intent
            movieIdFromExtra = b.getInt("resultItemId")
        }

        resultDisposable = homeViewModel.getMovies().subscribe({

            checkInternetDisposable = hasInternetConnection().subscribe { hasInternet ->
                if(hasInternet) {
                    //fetch data from url and insert into room
                    // on recupère la video depuis l'api
                    fetchVideoDisposable =
                        homeViewModel.fetchVideoByMovieId(resultItem?.id.toString()).subscribe()

                    // on recupère le cast
                    fetchCreditDisposable =
                        homeViewModel.fetchCreditByMovieId(resultItem?.id.toString()).subscribe()

                    //on recupère la liste des films similaires
                    fetchSimilarDisposable =
                        homeViewModel.fetchSimilarMovieByMovieId(resultItem?.id.toString()).subscribe()
                } else {
                    // on fait disparaitre les view correspondante

                }
            }

            it.forEach {
                it.results.forEach { result ->
                    if (result.id == movieIdFromExtra) {
                        resultItem = result

                        // ici on affiche la petite image avec l'animation
                        Picasso.get()
                            .load("https://image.tmdb.org/t/p/w200/${resultItem?.poster_path}")
                            .fit()
                            .noFade()
                            .centerCrop()
                            .placeholder(R.drawable.placeholder)
                            .into(bannerMovie, object : Callback {
                                override fun onError(e: Exception?) {
                                    supportStartPostponedEnterTransition()
                                }

                                override fun onSuccess() {
                                    supportStartPostponedEnterTransition()
                                }
                            })

                        // ici on affiche l'image de fond
                        Picasso.get()
                            .load("https://image.tmdb.org/t/p/w400/${resultItem?.backdrop_path}")
                            .placeholder(R.drawable.placeholder)
                            .into(bannerBack)

                        starTv?.text = resultItem?.vote_average.toString()
                        releaseDateTv?.text = resultItem?.release_date

                        //on va chercher tout les genrs disponibles
                        getGenreFromRoomDisposable = homeViewModel.getAllGenre().subscribe { allGenre ->
                            // on boucle sur les genre du film
                            resultItem?.genre_ids?.forEach { idItem ->
                                // on boucle sur tout les genre pour check si un genre correspond
                                allGenre.forEach {
                                    if (it.id == idItem) {
                                        // on les genres qui correspond dans la liste
                                        listGenre?.add(it)
                                    }
                                }
                            }
                            // ici on actualise la liste des genre et on notif l'adapter
                            genreAdapter?.genreList = listGenre!!
                            genreAdapter?.notifyDataSetChanged()
                        }
                    }
                }
            }

            // on met la description
            descMovieTv?.text = resultItem?.overview

            getCreditDisposable = homeViewModel.getCreditById(resultItem?.id!!).subscribe {
                castAdapter?.castList = it.cast
                castAdapter?.notifyDataSetChanged()
                Log.e("credit", it.toString())
            }

            //on recupère les video similaire depuis room
            getSimilarDisposable = homeViewModel.getSimilarMovieById(resultItem?.id!!).subscribe {

                // on set up l'adapter pour les movie similaires
                viewPagerAdapterSimilarMovie = SimilarMovieListAdapter(it.results)
                viewPagerSimilarMovie?.adapter = viewPagerAdapterSimilarMovie

                btnNextPager?.setOnClickListener {
                    // on passe a l'image suivante
                    viewPagerSimilarMovie?.setCurrentItem(
                        viewPagerSimilarMovie?.currentItem!! + 1,
                        true
                    )
                }

                btnPrevPager?.setOnClickListener {
                    // on passe a l'image precedente
                    viewPagerSimilarMovie?.setCurrentItem(
                        viewPagerSimilarMovie?.currentItem!! - 1,
                        true
                    )
                }

                viewPagerSimilarMovie?.registerOnPageChangeCallback(object :
                    ViewPager2.OnPageChangeCallback() {
                    override fun onPageScrolled(
                        position: Int,
                        positionOffset: Float,
                        positionOffsetPixels: Int
                    ) {
                        super.onPageScrolled(position, positionOffset, positionOffsetPixels)
                        //on rend invisible les boutons selon la position
                        if (position == 0) btnPrevPager?.visibility =
                            View.GONE else btnPrevPager?.visibility = View.VISIBLE
                        if (position == viewPagerAdapterSimilarMovie?.itemCount!! - 1) btnNextPager?.visibility =
                            View.GONE else btnNextPager?.visibility = View.VISIBLE
                    }

                    override fun onPageSelected(position: Int) {
                        super.onPageSelected(position)

                        Log.e("Selected_Page", position.toString())
                    }

                    override fun onPageScrollStateChanged(state: Int) {
                        super.onPageScrollStateChanged(state)
                    }
                })
            }

            // on recupère la video depuis room
            getVideoDisposable = homeViewModel.getVideoByMovieId(resultItem?.id!!).subscribe { video ->
                if (video.results.isEmpty()) {
                    btnPlay?.visibility = View.GONE
                } else {
                    btnPlay?.setOnClickListener {
                        val key = video.results[0].key
                        val url = VIDEO_TRAILER_BASE + key
                        val i = Intent(Intent.ACTION_VIEW)
                        i.data = Uri.parse(url)
                        startActivity(i)
                    }
                }
            }
        }, {
            Log.e("detailError", it.message)
        })


    }


    private fun hasInternetConnection(): Single<Boolean> {
        return Single.fromCallable {
            try {
                // Connect to Google DNS to check for connection
                val timeoutMs = 1500
                val socket = Socket()
                val socketAddress = InetSocketAddress("8.8.8.8", 53)

                socket.connect(socketAddress, timeoutMs)
                socket.close()

                true
            } catch (e: IOException) {
                false
            }
        }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    override fun onDestroy() {
        super.onDestroy()
        resultDisposable?.dispose()
        getGenreFromRoomDisposable?.dispose()
        fetchCreditDisposable?.dispose()
        fetchVideoDisposable?.dispose()
        fetchSimilarDisposable?.dispose()
        getVideoDisposable?.dispose()
        getSimilarDisposable?.dispose()
        getCreditDisposable?.dispose()
        checkInternetDisposable?.dispose()
    }
}