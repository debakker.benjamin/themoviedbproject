package com.example.moviesapp.utils

object Constants {
    const val BASE_URL = "http://api.themoviedb.org/3/"
    const val API_KEY = "089be7e3a420824a38a39156ca46991e"
    const val DATABASE_NAME = "movie_db"
    const val VIDEO_TRAILER_BASE = "https://www.youtube.com/watch?v="
}