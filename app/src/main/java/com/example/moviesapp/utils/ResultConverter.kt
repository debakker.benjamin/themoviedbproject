package com.example.moviesapp.utils

import androidx.room.TypeConverter
import com.example.moviesapp.entities.Result
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class ResultConverter {
        @TypeConverter
        fun toResults(data: String?): List<Result> {
            val gson = Gson()
            if (data == null) {
                return emptyList<Result>()
            }
            val listType = object : TypeToken<List<Result>>() {

            }.type
            return gson.fromJson(data, listType)
        }

        @TypeConverter
        fun toString(myObjects: List<Result>): String {
            val gson = Gson()
            return gson.toJson(myObjects)
        }
}
