package com.example.moviesapp.utils

import androidx.room.TypeConverter
import com.example.moviesapp.entities.Cast
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class CastConverter {
    @TypeConverter
    fun toResults(data: String?): List<Cast> {
        val gson = Gson()
        if (data == null) {
            return emptyList<Cast>()
        }
        val listType = object : TypeToken<List<Cast>>() {

        }.type
        return gson.fromJson(data, listType)
    }

    @TypeConverter
    fun toString(myObjects: List<Cast>): String {
        val gson = Gson()
        return gson.toJson(myObjects)
    }
}