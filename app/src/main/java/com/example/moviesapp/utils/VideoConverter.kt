package com.example.moviesapp.utils

import androidx.room.TypeConverter
import com.example.moviesapp.entities.ResultVideo
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken


class VideoConverter {
    @TypeConverter
    fun toResults(data: String?): List<ResultVideo> {
        val gson = Gson()
        if (data == null) {
            return emptyList<ResultVideo>()
        }
        val listType = object : TypeToken<List<ResultVideo>>() {

        }.type
        return gson.fromJson(data, listType)
    }

    @TypeConverter
    fun toString(myObjects: List<ResultVideo>): String {
        val gson = Gson()
        return gson.toJson(myObjects)
    }
}