package com.example.moviesapp.utils

import android.widget.ImageView
import com.example.moviesapp.entities.Result

interface ClickListener {
    fun onCardClicked(pos: Int, resultItem: Result, shareImageView: ImageView)
}
