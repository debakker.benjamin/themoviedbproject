package com.example.moviesapp.fragment

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.moviesapp.R
import com.example.moviesapp.adapter.FavoriteMovieListAdapter
import com.example.moviesapp.adapter.MovieListAdapter
import com.example.moviesapp.entities.Result
import com.example.moviesapp.utils.EndlessRecyclerViewScrollListener
import com.example.moviesapp.viewmodel.HomeViewModel
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.io.IOException
import java.net.InetSocketAddress
import java.net.Socket

class FavoriteFragment : Fragment() {

    private var recyclerMovie: RecyclerView? = null
    private var movieAdapter: FavoriteMovieListAdapter? = null
    private var fetchSubscribe: Disposable? = null
    private var fetchGenreSubscribe: Disposable? = null
    private var getDataFromRoomDisposable: Disposable? = null
    private var checkInternetDisposable: Disposable? = null
    var listResult: List<Result>? = null
    private lateinit var gridManager: GridLayoutManager
    private var noFavTv: TextView? = null

    private val homeViewModel by lazy {
        ViewModelProviders.of(this).get(HomeViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.favorite_fragment, container, false)

        noFavTv = view.findViewById(R.id.no_fav_tv)


        //set up recyclerview
        recyclerMovie = view.findViewById(R.id.recycler_movies)
        listResult = ArrayList()

        //setup adapter and gridmanager
        movieAdapter = FavoriteMovieListAdapter(listResult!!, context!!, activity!!, this)
        gridManager = GridLayoutManager(context, 2)
        recyclerMovie?.layoutManager = gridManager
        recyclerMovie?.adapter = movieAdapter

        //check if internet is online
        checkInternetDisposable = hasInternetConnection().subscribe { hasInternet ->
            if (hasInternet) {
                //fetch data from url and insert into room
                fetchSubscribe = homeViewModel.fetchMovieByPage("1").subscribe()
                fetchGenreSubscribe = homeViewModel.fetchGenre().subscribe()

                // on affiche le reste des pages si on a internet :)
                val scrollListener = object : EndlessRecyclerViewScrollListener(gridManager) {
                    override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView) {
                        fetchSubscribe = homeViewModel.fetchMovieByPage(page.toString()).subscribe()
                    }
                }
                recyclerMovie?.addOnScrollListener(scrollListener)
            }
        }

        val listIdFavoriteMovie = ArrayList<String>()
        val pref = activity?.getSharedPreferences("MoviePref", Context.MODE_PRIVATE)
        val sharedPreferenceIds = pref?.all?.map {
            listIdFavoriteMovie.add(it.key)
        }

        //get data from room
        getDataFromRoomDisposable = homeViewModel.getMovies().subscribe({
            val resultList = ArrayList<Result>()

            it.forEach { itemMovie ->
                itemMovie.results.forEach { results ->
                    listIdFavoriteMovie.forEach {
                        if (it == results.id.toString()) {
                            resultList.add(results)
                        }
                    }
                }
            }

            movieAdapter?.resultList = resultList
            movieAdapter?.notifyDataSetChanged()

            if (movieAdapter?.resultList?.isEmpty()!!) {
                setTvNoFavVisibility(true)
            } else {
                setTvNoFavVisibility(false)
            }
        }, {
            Log.e("fetchDataError", it.message)
        })


        return view
    }

    fun setTvNoFavVisibility(visibility: Boolean) {
        if (visibility) noFavTv?.visibility = View.VISIBLE else noFavTv?.visibility = View.GONE
    }

    override fun onResume() {
        super.onResume()

        movieAdapter?.notifyDataSetChanged()
    }

    override fun onDestroy() {
        super.onDestroy()
        fetchSubscribe?.dispose()
        fetchGenreSubscribe?.dispose()
        getDataFromRoomDisposable?.dispose()
    }


    private fun hasInternetConnection(): Single<Boolean> {
        return Single.fromCallable {
            try {
                // Connect to Google DNS to check for connection
                val timeoutMs = 1500
                val socket = Socket()
                val socketAddress = InetSocketAddress("8.8.8.8", 53)

                socket.connect(socketAddress, timeoutMs)
                socket.close()

                true
            } catch (e: IOException) {
                false
            }
        }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    companion object {
        fun newInstance(): FavoriteFragment = FavoriteFragment()
        val TAG = "Favoris fragment"

    }
}