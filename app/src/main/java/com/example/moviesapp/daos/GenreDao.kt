package com.example.moviesapp.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.moviesapp.entities.Genre
import io.reactivex.Flowable

@Dao
interface GenreDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertGenreMovies(genre: Genre)

    @Query("SELECT * FROM Genre WHERE id=:id")
    fun getGenreMovieByid(id: Int) : Flowable<Genre>

    @Query("SELECT * FROM Genre")
    fun getAllGenre(): Flowable<List<Genre>>
}