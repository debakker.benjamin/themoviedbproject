package com.example.moviesapp.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.moviesapp.entities.Video
import io.reactivex.Flowable

@Dao
interface VideoDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertVideo(video: Video)

    @Query("SELECT * FROM Video WHERE id= :id")
    fun getVideoById(id: Int): Flowable<Video>
}