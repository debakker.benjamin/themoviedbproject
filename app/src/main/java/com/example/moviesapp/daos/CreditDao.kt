package com.example.moviesapp.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.moviesapp.entities.Credit
import io.reactivex.Flowable

@Dao
interface CreditDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCredit(credit: Credit)

    @Query("SELECT * FROM Credit")
    fun getAllCredit(): Flowable<List<Credit>>

    @Query("SELECT * FROM Credit WHERE id= :id")
    fun getCreditById(id: Int): Flowable<Credit>
}