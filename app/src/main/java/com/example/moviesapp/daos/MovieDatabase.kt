package com.example.moviesapp.daos

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.moviesapp.entities.*
import com.example.moviesapp.utils.CastConverter
import com.example.moviesapp.utils.Constants
import com.example.moviesapp.utils.ResultConverter
import com.example.moviesapp.utils.VideoConverter
import fr.lam.parc.local.util.SingletonHolder

@Database(entities = arrayOf(ItemMovie::class, Result::class, Genre::class, Credit::class, SimilarMovie::class, Video::class), version = 1, exportSchema = false)
@TypeConverters(ResultConverter::class, CastConverter::class, VideoConverter::class)
abstract class MovieDatabase : RoomDatabase() {

    abstract fun movieDao(): MovieDao

    abstract fun genreDao(): GenreDao

    abstract fun creditDao(): CreditDao

    abstract fun similarMovieDao(): SimilarMovieDao

    abstract fun videoDao(): VideoDao

    companion object : SingletonHolder<MovieDatabase, Context>({
        Room.databaseBuilder(it.applicationContext,
            MovieDatabase::class.java, Constants.DATABASE_NAME)
            .build()
    })
}