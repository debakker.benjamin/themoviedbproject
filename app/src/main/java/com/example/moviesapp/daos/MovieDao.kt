package com.example.moviesapp.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.moviesapp.entities.Genre
import com.example.moviesapp.entities.ItemMovie
import com.example.moviesapp.entities.Result
import io.reactivex.Flowable
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.Single

@Dao
interface MovieDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun inserAllMovies(itemMovie: ItemMovie)

    @Query("SELECT * FROM Itemmovie")
    fun getAllMovies() : Flowable<List<ItemMovie>>
}