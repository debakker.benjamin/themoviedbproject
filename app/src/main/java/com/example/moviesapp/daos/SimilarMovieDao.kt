package com.example.moviesapp.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.moviesapp.entities.SimilarMovie
import io.reactivex.Flowable

@Dao
interface SimilarMovieDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertSimilarMovie(similarMovie: SimilarMovie)

    @Query("SELECT * FROM SimilarMovie")
    fun getAllSimilarMovie(): Flowable<SimilarMovie>

    @Query("SELECT * FROM SimilarMovie WHERE movieId= :id")
    fun getSimilarMovieById(id: Int): Flowable<SimilarMovie>
}