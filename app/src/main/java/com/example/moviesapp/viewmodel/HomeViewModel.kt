package com.example.moviesapp.viewmodel

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import com.example.moviesapp.daos.MovieDatabase
import com.example.moviesapp.entities.*
import com.example.moviesapp.service.DataRepository
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class HomeViewModel(app: Application): AndroidViewModel(app) {

    private val repository = DataRepository()

    val database = MovieDatabase.getInstance(getApplication())

    fun fetchMovieByPage(page : String): Observable<Int> {
        return repository.getPopularMovie(page)
            .flatMap {
                Log.d("movieertvrtvrztvz", it.results.toString())
                database.movieDao().inserAllMovies(it)
                Observable.just(1)
            }
            .doOnError {
                Log.e("error", "Fail to load Data")
            }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun fetchGenre(): Observable<Int>{
        return repository.getGenreMovie()
            .flatMap {
                Log.d("genres", it.toString())
                it.genres.forEach { genre ->
                    database.genreDao().insertGenreMovies(genre)
                }
                Observable.just(1)
            }
            .doOnError {
                Log.e("error", "Fail to load Data")
            }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun fetchCreditByMovieId(id: String): Observable<Int>{
        return repository.getCreditByMovieId(id)
            .flatMap {
                Log.d("credit", it.cast.toString())
                database.creditDao().insertCredit(it)
                Observable.just(1)
            }
            .doOnError {
                Log.e("error", "Fail to load Data Credit")
            }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun fetchSimilarMovieByMovieId(id: String) : Observable<Int>{
        return  repository.getSimilarMovieByMovieId(id)
            .flatMap {
                Log.d("similarMovie", it.results.toString())
                it.movieId = id.toInt()
                database.similarMovieDao().insertSimilarMovie(it)
                Observable.just(1)
            }
            .doOnError {
                Log.e("error", "Fail to load Data Similar Movies")
            }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun fetchVideoByMovieId(id: String) : Observable<Int>{
        return repository.getVideoByMovieId(id)
            .flatMap {
                Log.e("video", it.results.toString())
                database.videoDao().insertVideo(it)
                Observable.just(1)
            }
            .doOnError {
                Log.e("error", "Fail to load Data Video")
            }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun getVideoByMovieId(id: Int): Flowable<Video>{
        return database.videoDao().getVideoById(id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun getSimilarMovieById(id: Int): Flowable<SimilarMovie>{
        return database.similarMovieDao().getSimilarMovieById(id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun getCreditById(id: Int): Flowable<Credit>{
        return database.creditDao().getCreditById(id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun getMovies() : Flowable<List<ItemMovie>>{
        return database.movieDao().getAllMovies()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun getGenreMovies(id:Int): Flowable<Genre>{
        return database.genreDao().getGenreMovieByid(id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun getAllGenre(): Flowable<List<Genre>>{
        return database.genreDao().getAllGenre()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }
}