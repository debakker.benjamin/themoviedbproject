package com.example.moviesapp.service

import com.example.moviesapp.entities.*
import io.reactivex.Observable

class DataRepository {

    private val apiService: ApiService by lazy {
        ApiService.create()
    }

    fun getPopularMovie(page: String): Observable<ItemMovie> {
        return apiService.getPopularMovies(page)
    }

    fun getGenreMovie(): Observable<Genres>{
        return apiService.getGenreMovies()
    }

    fun getCreditByMovieId(id: String): Observable<Credit>{
        return apiService.getCreditByMovieId(id)
    }

    fun getSimilarMovieByMovieId(id: String): Observable<SimilarMovie>{
        return apiService.getSimilarMovieById(id)
    }

    fun getVideoByMovieId(id: String): Observable<Video>{
        return  apiService.getVideoByMovieId(id)
    }
}
