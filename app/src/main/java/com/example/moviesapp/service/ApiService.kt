package com.example.moviesapp.service

import com.example.moviesapp.entities.*
import com.example.moviesapp.utils.Constants.API_KEY
import com.example.moviesapp.utils.Constants.BASE_URL
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {

    @GET("movie/popular?api_key=$API_KEY&language=fr-FR")
    fun getPopularMovies(@Query(value = "page", encoded = true) page: String): Observable<ItemMovie>

    @GET("genre/movie/list?api_key=$API_KEY&language=fr-FR")
    fun getGenreMovies(): Observable<Genres>

    @GET("movie/{id}/credits?api_key=$API_KEY")
    fun getCreditByMovieId(@Path(value = "id", encoded = true) movieId: String): Observable<Credit>

    @GET("movie/{id}/similar?api_key=$API_KEY&language=fr-FR")
    fun getSimilarMovieById(@Path(value = "id", encoded = true) movieId: String): Observable<SimilarMovie>

    @GET("movie/{id}/videos?api_key=$API_KEY&language=fr-FR")
    fun getVideoByMovieId(@Path(value = "id", encoded = true) movieId: String) : Observable<Video>

    companion object {
        fun create(): ApiService{
            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                //.client(HelperService.instance.client)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()

            return retrofit.create(ApiService::class.java)
        }
    }
}