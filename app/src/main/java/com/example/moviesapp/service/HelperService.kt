package com.example.moviesapp.service

import com.example.moviesapp.utils.Constants.API_KEY
import okhttp3.OkHttpClient

class HelperService {
    val client: OkHttpClient

    init {
        val httpClient = OkHttpClient.Builder()
        httpClient.addInterceptor { chain ->
            val original = chain.request()
            var requestBuilder = original.newBuilder()
            requestBuilder =
                requestBuilder.header("api_key", API_KEY)
            val request = requestBuilder.build()

            chain.proceed(request)
        }
        client = httpClient.build()
    }

    companion object {
        val instance: HelperService by lazy { HelperService() }
    }
}
