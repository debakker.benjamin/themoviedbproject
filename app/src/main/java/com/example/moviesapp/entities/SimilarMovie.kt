package com.example.moviesapp.entities

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.example.moviesapp.utils.ResultConverter

@Entity
data class SimilarMovie(
    @PrimaryKey
    var movieId: Int,

    var page: Int,

    @TypeConverters(ResultConverter::class)
    var results: List<Result>,

    var total_pages: Int,
    var total_results: Int
) {
    constructor(): this(0, 0, ArrayList() ,0, 0)
}