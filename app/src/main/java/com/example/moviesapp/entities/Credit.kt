package com.example.moviesapp.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.example.moviesapp.utils.CastConverter

@Entity
data class Credit(
    @PrimaryKey
    var id: Int,
    @TypeConverters(CastConverter::class)
    var cast: List<Cast>
) {
    constructor(): this(0, ArrayList<Cast>())
}