package com.example.moviesapp.entities

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity
data class Result(
    @PrimaryKey
    var id: Int,
    var poster_path: String,
    var adult: Boolean,
    var overview: String,
    var release_date: String,
    @Ignore
    var genre_ids: ArrayList<Int>,
    var original_title: String,
    var original_language: String,
    var title: String,
    var backdrop_path: String,
    var popularity: Double,
    var vote_count: Int,
    var video: Boolean,
    var vote_average: Double
) : Serializable {
    constructor() : this(0,"",false,"","", ArrayList<Int>(),"","","","",0.0,0,false,0.0)
}