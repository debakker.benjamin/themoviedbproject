package com.example.moviesapp.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Genres(
    @PrimaryKey
    var genres: List<Genre>
) {
    constructor(): this(ArrayList<Genre>())
}