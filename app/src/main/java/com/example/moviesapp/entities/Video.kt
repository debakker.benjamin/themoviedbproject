package com.example.moviesapp.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.example.moviesapp.utils.VideoConverter

@Entity
data class Video(
    @PrimaryKey
    var id: Int,

    @ColumnInfo(name = "results")
    @TypeConverters(VideoConverter::class)
    var results: List<ResultVideo>

) {
    constructor(): this(0, ArrayList())
}