package com.example.moviesapp.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverter
import androidx.room.TypeConverters
import com.example.moviesapp.utils.ResultConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.util.*
import java.util.Collections.emptyList
import kotlin.collections.ArrayList


@Entity
data class ItemMovie(
    @PrimaryKey
    var page: Int,

    var total_result: Int,

    var total_pages: Int,

    @TypeConverters(ResultConverter::class)
    var results: List<Result>
) {
    constructor(): this(0,0,0,ArrayList<Result>())
}

