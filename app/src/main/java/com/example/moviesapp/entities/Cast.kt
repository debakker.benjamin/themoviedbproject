package com.example.moviesapp.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Cast(
    @PrimaryKey
    var id: Int,
    var name: String,
    var charachter: String,
    var credit_id: String,
    var genre: Int,
    var order: Int,
    var profile_path: String

) {
    constructor(): this(0, "","","",0,0, "")
}