package com.example.moviesapp.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Genre(
    @PrimaryKey
    var id: Int,
    var name: String
) {
    constructor(): this(0, "")
}